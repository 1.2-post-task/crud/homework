@extends('account.layout')
@section('content')

<div class="pull-left">
    <h2 class="text-center pt-5">GENSHIN ACCOUNT CRUD</h2>
</div>

<div class="row1">
    <div class="col-lg-12 margin-tb pt-3 pb-3">
        <div class="pull-right">
            <a class="btn btn-success" href="{{ route('account.create') }}"> Create Account</a>
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<div class="tbl">
    <div class="container">
        <table class="table table-bordered">
            <tr>
                <th>ID</th>
                <th>Account</th>
                <th>Level</th>
                <th>Email</th>
            </tr>

            @foreach ($account as $Account)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $Account->account }}</td>
                <td>{{ $Account->lvl }}</td>
                <td>{{ $Account->email }}</td>
                <td>
                    <form action="{{ route('account.destroy',$Account->id) }}" method="POST">
                        <a class="btn btn-primary" href="{{ route('account.edit',$Account->id) }}">Edit</a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</div>