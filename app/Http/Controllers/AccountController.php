<?php

namespace App\Http\Controllers;

use App\Models\Account;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function index()
    {
        $account=Account::latest()->paginate(5);
        return view ('account.index',compact('account'))
        ->with('i',(request()->input('page',1)-1)*5);
    
    }
    
    public function create()
    {
        return view('account.create');
    }
    
    
    public function store(Request $request)
    {
        $request->validate([
        'account'=>'required',
        'lvl'=>'required',
        'email'=>'required',
    
    ]);
        Account::create($request->all());
    
        return redirect()->route('account.index')
        ->with('Success', 'Account has been created successfully!');
    }
    
    
    public function edit(Account $account)
    {
    
        return view('account.edit',compact('account'));
    }
    
        public function update(Request $request,Account $account)
    {
        $request->validate([
    
    ]);
    
        $account->update($request->all());
    
        return redirect()->route('account.index')
        ->with('Success','Account has been updated!');
    
    }
    
        public function destroy(Account $account)
    {
        $account->delete();
    
        return redirect()->route('account.index')
        ->with('Success','Account has been removed!');
    
    }
}
